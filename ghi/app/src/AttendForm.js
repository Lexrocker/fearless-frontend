import React from 'react';

class AttendForm extends React.Component {

  
  // JS doesn't handle objects and methods the same way python does
  // If you refer to an object's method (like a property), JS won't
  // remember what object it came from. So if we are using a method as
  // an even handler (like HandleNameChange) we need this
  // constructor - bind 'thing'
  constructor(props){
    super(props);
    this.state = {
      name: '',
      email: '',
      conferences: []
    };

    this.handleNameChange = this.handleNameChange.bind(this);
    this.handleEmailChange = this.handleEmailChange.bind(this);
    this.handleConferenceChange = this.handleConferenceChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

    async componentDidMount() {
        const url = 'http://localhost:8000/api/conferences/';
    
        const response = await fetch(url);
        if (response.ok) {
            
            const data = await response.json();
            //console.log('help: ',data)
        
            // the react way: 

            // set state adds the data to the React component
            // and reloads the component's HTML
            // the rest of the change is in the <select> tag in
            // the JSX html in the render() below
            this.setState({conferences: data.conferences});
            

            //the non-react way (below):
            // const selectTag = document.getElementById('state');
            // for (let state of data.states) {
            //     const option = document.createElement('option');
            //     option.value = state.abbreviation;
            //     option.innerHTML = state.name;
            //     selectTag.appendChild(option);
            // }
        }
    }

    handleNameChange(event) {
      // event is the event that occured, target is the tag that caused the event
      // value property is the value in the input
      const value = event.target.value;
      this.setState({name: value});
      //console.log(value); //just for fun
    }
    
    
    handleEmailChange(event) {
      const value = event.target.value;
      this.setState({email: value});
      //console.log(value); //just for fun
    }


    handleConferenceChange(event) {
      const value = event.target.value;
      this.setState({conference: value});
      //console.log(value); //just for fun
    }
    
    
    async handleSubmit(event){
      event.preventDefault();
      const data = {...this.state};

      console.log('data: ',data)

      // hacky way of getting location
      // turns href string into the location number
      // turns "/api/locations/2/" => "2"
      let hold = data.conference.replace(/\D+/, ''); //removes everyting but a / at the end
      hold = hold.slice(0,-1); // removes the / at the end
      
    
      const attendeeUrl = 'http://localhost:8001/api/conferences/' + hold + '/attendees/';
      console.log('hold:', hold)
      console.log('url:', attendeeUrl)


      //delete data.conference;
      delete data.conferences;


      let test = JSON.stringify(data);
      //console.log(test);
      
      const fetchConfig = {
        method: "post",
        body: JSON.stringify(data),
        headers: {
          'Content-Type': 'application/json',
        },
      };
      const response = await fetch(attendeeUrl, fetchConfig);
      if (response.ok) {
        const newConference = await response.json();
        //console.log(newConference);
    
        const cleared = {
          name: '',
          roomCount: '',
          city: '',
          state: '',
        };
        this.setState(cleared);
      }
    }

    render() {
        return (
            <div className="row">
            <div className="offset-3 col-6">
              <div className="shadow p-4 mt-4">
                <h1>Alex Rocks</h1>
                <form onSubmit={this.handleSubmit} id="create-location-form">
                  <div className="form-floating mb-3">
                    <input onChange={this.handleNameChange} value={this.state.name} placeholder="Name" required type="text" name ="name" id="name" className="form-control"/>
                    <label htmlFor="name">Name</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input onChange={this.handleEmailChange} value ={this.state.email} placeholder="email" required type="email" name="email" id="email" className="form-control"/>
                    <label htmlFor="room_count">Email</label>
                  </div>
                  <div className="mb-3">
                    <select onChange={this.handleConferenceChange} value={this.state.location} required name="locations" id='locations' className='form-select'>
                    <option value="">Choose a location</option>
                      {this.state.conferences.map(conference => {
                       return (
                        <option key={conference.href} value={conference.href}>
                          {conference.name}
                        </option>
                      ); 
                     })}
                    </select>
                  </div>
                  <button className="btn btn-primary">Create</button>
                </form>
              </div>
            </div>
          </div>
        );
    };
};

export default AttendForm;