import React from 'react';

class ConferenceForm extends React.Component {

  
  // JS doesn't handle objects and methods the same way python does
  // If you refer to an object's method (like a property), JS won't
  // remember what object it came from. So if we are using a method as
  // an even handler (like HandleNameChange) we need this
  // constructor - bind 'thing'
  constructor(props){
    super(props);
    this.state = {
      name: '',
      starts: '',
      ends: '',
      description: '',
      maxPresentations: '',
      maxAttendees: '',
      locations: []
    };

    this.handleNameChange = this.handleNameChange.bind(this);
    this.handleStartChange = this.handleStartChange.bind(this);
    this.handleEndChange = this.handleEndChange.bind(this);
    this.handleDescriptionChange = this.handleDescriptionChange.bind(this);
    this.handleMaxPresentationsChange = this.handleMaxPresentationsChange.bind(this);
    this.handleMaxAttendeesChange = this.handleMaxAttendeesChange.bind(this);
    this.handleLocationChange = this.handleLocationChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

    async componentDidMount() {
        const url = 'http://localhost:8000/api/locations/';
    
        const response = await fetch(url);
    
        if (response.ok) {
            
            const data = await response.json();
            console.log('help: ',data)
        
            // the react way: 

            // set state adds the data to the React component
            // and reloads the component's HTML
            // the rest of the change is in the <select> tag in
            // the JSX html in the render() below
            this.setState({locations: data.locations});
            

            //the non-react way (below):
            // const selectTag = document.getElementById('state');
            // for (let state of data.states) {
            //     const option = document.createElement('option');
            //     option.value = state.abbreviation;
            //     option.innerHTML = state.name;
            //     selectTag.appendChild(option);
            // }
        }
    }

    handleNameChange(event) {
      // event is the event that occured, target is the tag that caused the event
      // value property is the value in the input
      const value = event.target.value;
      this.setState({name: value});
      //console.log(value); //just for fun
    }
    
    
    handleStartChange(event) {
      const value = event.target.value;
      this.setState({starts: value});
      //console.log(value); //just for fun
    }
    
    
    handleEndChange(event) {
      const value = event.target.value;
      this.setState({ends: value});
      //console.log(value); //just for fun
    }
    
    
    handleDescriptionChange(event) {
      // event is the event that occured, target is the tag that caused the event
      // value property is the value in the input
      const value = event.target.value;
      this.setState({description: value});
      //console.log(value); //just for fun
    }
    
    
    handleMaxPresentationsChange(event) {
      const value = event.target.value;
      this.setState({maxPresentations: value});
      //console.log(value); //just for fun
    }
    
    handleMaxAttendeesChange(event) {
      const value = event.target.value;
      this.setState({maxAttendees: value});
      //console.log(value); //just for fun
    }

    handleLocationChange(event) {
      const value = event.target.value;
      this.setState({location: value});
      //console.log(value); //just for fun
    }
    
    
    async handleSubmit(event){
      event.preventDefault();
      const data = {...this.state};
     
      //converting naming converntions
      data.max_presentations = data.maxPresentations;
      data.max_attendees = data.maxAttendees;

      const conferenceUrl = 'http://localhost:8000/api/conferences/';


      // hacky way of getting location
      // turns href string into the location number
      // turns "/api/locations/2/" => "2"
      let hold = data.location.replace(/\D+/, ''); //removes everyting but a / at the end
      hold = hold.slice(0,-1); // removes the / at the end
      data.location = hold; // sets the location = the hold
      
      delete data.locations;
      delete data.maxPresentations;
      delete data.maxAttendees;

      let test = JSON.stringify(data);
      //console.log(test);
      
      const fetchConfig = {
        method: "post",
        body: JSON.stringify(data),
        headers: {
          'Content-Type': 'application/json',
        },
      };
      const response = await fetch(conferenceUrl, fetchConfig);
      if (response.ok) {
        const newConference = await response.json();
        console.log(newConference);
    
        const cleared = {
          name: '',
          roomCount: '',
          city: '',
          state: '',
        };
        this.setState(cleared);
      }
    }

    render() {
        return (
            <div className="row">
            <div className="offset-3 col-6">
              <div className="shadow p-4 mt-4">
                <h1>Create a new conference</h1>
                <form onSubmit={this.handleSubmit} id="create-location-form">
                  <div className="form-floating mb-3">
                    <input onChange={this.handleNameChange} value={this.state.name} placeholder="Name" required type="text" name ="name" id="name" className="form-control"/>
                    <label htmlFor="name">Name</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input onChange={this.handleStartChange} value ={this.state.starts} placeholder="starts" required type="date" name="starts" id="starts" className="form-control"/>
                    <label htmlFor="room_count">Starts</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input onChange={this.handleEndChange} value={this.state.ends} placeholder="ends" required type="date" name="ends" id="ends" className="form-control"/>
                    <label htmlFor="city">Ends</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input onChange={this.handleDescriptionChange} value={this.state.description} placeholder="description" required type="text" name ="description" id="description" className="form-control"/>
                    <label htmlFor="name">Description</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input onChange={this.handleMaxPresentationsChange} value={this.state.maxPresentations} placeholder="Maximum Presentations" required type="number" min="0" name ="maxPresentations" id="maxPresentations" className="form-control"/>
                    <label htmlFor="name">Maximum presentations</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input onChange={this.handleMaxAttendeesChange} value={this.state.maxAttendees} placeholder="Maximum Attendees" required type="number" min="1" name ="maxAttendees" id="maxAttendees" className="form-control"/>
                    <label htmlFor="name">Maximum attendees</label>
                  </div>
                  <div className="mb-3">
                    <select onChange={this.handleLocationChange} value={this.state.location} required name="locations" id='locations' className='form-select'>
                    <option value="">Choose a location</option>
                      {this.state.locations.map(location => {
                       return (
                        <option key={location.href} value={location.href}>
                          {location.name}
                        </option>
                      );
                     })}
                    </select>
                  </div>
                  <button className="btn btn-primary">Create</button>
                </form>
              </div>
            </div>
          </div>
        );
    };
};

export default ConferenceForm;