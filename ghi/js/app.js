function createCard(name, description, pictureUrl, place) {
    return `
        <div class = "col">
            <div class="card">
                <img src="${pictureUrl}" class="card-img-top">
                    <div class="card-body"> 
                    <h5 class="card-title">${name}</h5>
                    <h6 class="card-subtitle mb-2 text-muted">${place}</h6>
                <p class="card-text">${description}</p>
                </div>
            </div>
        </div>
        `;
}
    
    
window.addEventListener('DOMContentLoaded', async () => {
        
    const url = 'http://localhost:8000/api/conferences';
        
    try {
            const response = await fetch(url);
            
            if (!response.ok) {
                // Figure out what to do when the response is bad
                console.error("The call had an error")
            } else {
                
                
                const data = await response.json();
                
                let r = 0;
                let holdHTML = "";
                let hold3HTML = [];
                let column = document.querySelector('.replace');
                
            for (let conference of data.conferences) {
                const detailUrl = `http://localhost:8000${conference.href}`;
                const detailResponse = await fetch(detailUrl);
                if (detailResponse.ok) {
                    const details = await detailResponse.json();
                    const title = details.conference.name;
                    const description = details.conference.description;
                    const pictureUrl = details.conference.location.picture_url;
            
                    const startDate = new Date(details.conference.starts);
                    const star = startDate.toLocaleDateString()
                    const endDate = new Date(details.conference.ends);
                    const end = endDate.toLocaleDateString()
                    const place = details.conference.location.name;
                    const html = createCard(
                        title,
                        description,
                        pictureUrl,
                        star,
                        end,
                        place);
                
                    // r++;

                    // // attempt to create single HTML string for querySelector
                    // if (r === 3){
                    //     r = 0;
                    //     holdHTML.concat(html); // add html to hold string
                    //     hold3HTML.push(holdHTML); // store 3 stored html strings to hold3
                    //     holdHTML = ""; // empty html hold
                    // } else {
                    //     holdHTML.concat(html); // add html to hold string
                    // }
                    //     console.log(html.concat(html));

                column.innerHTML += html;

                console.log(column.innerHTML)
                console.log("test")

                    }
                }
            }
            
            // column = `<div class="row">`;
            // endDiv = `</div>`;
            // result = "";
            // for (let list in hold3HTML){
            //     result = result.concat(column);
            // }
            


                
                
                // let row = document.querySelector('.row');
                // row.innerHTML += html;

              
            
      
        //   }
            
        //     const conference = data.conferences[0];
        //     // console.log(data);
        //     const nameTag = document.querySelector('.card-title');
        //     nameTag.innerHTML = conference.name; // .innerHMTL Element property overides HTML markup and replaces it
            
            
        //     const detailUrl = `http://localhost:8000${conference.href}`;
        //     //console.log(detailUrl);
        //     const detailResponse = await fetch(detailUrl);
            
        //     if (detailResponse.ok) {
        //         // const details = await detailResponse.json();
        //         // const info = details.conference['description'];
        //         // const deTag = document.querySelector('.card-text');
        //         // deTag.innerHMTL = info;
        //         // console.log(info, details)
        //         const details = await detailResponse.json();

        //         const info = details.conference['description'];
        //         const detTag = document.querySelector('.card-text');
        //         detTag.innerHTML = info;
        //         // console.log(details);

        //         const imageTag = document.querySelector('.card-img-top');
        //         imageTag.src = details.conference.location.picture_url;


        //     

        } catch (e) {
            // Figure out what to do if an error is raised
            console.error("Fetch or URL had an error")
    

            let warning =   `<div class="alert alert-danger" role="alert">
                                Fetch or URL had an error!
                            </div>`;
            let column = document.querySelector('.replace');
            column.innerHTML += warning;
        };
    });
