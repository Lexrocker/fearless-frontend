window.addEventListener('DOMContentLoaded', async () => {


    console.log('addEventListener is active');
    const state_url = 'http://localhost:8000/api/states/';

    try {
        
        console.log('we are live');
        const data = await fetch(state_url);
        console.log(data);
        
        if (data.ok){
            const states = await data.json();
            console.log('the states data looks like: ', states);

            // Get the select tag element by its id 'state'
            const element = document.getElementById('state');

            
            for (let state of states.states){
            // For each state in the states property of the data
            // Create an 'option' element
            let option = document.createElement('option');

            // Set the '.value' property of the option element to the
            // state's abbreviation

            option.value = state.abbreviation;

            // Set the '.innerHTML' property of the option element to
            // the state's name

            option.innerHTML = state.name;

            // Append the option element as a child of the select tag
            element.appendChild(option);
            };
        };

    } catch {

        console.log('error during fetch / api data parsing');

    };

    const formTag = document.getElementById('create-location-form'); // getting form element
    formTag.addEventListener('submit', async event => { // waiting for user to submit a form via element
        event.preventDefault(); // prevent the browser from sending the data to our server
                                // instead we will have it send it to our RESTful API

        const formData = new FormData(formTag); // creating new formData object
        const json = JSON.stringify(Object.fromEntries(formData));
        console.log(json);  

        const locationUrl = 'http://localhost:8000/api/locations/';
        const fetchConfig = {
        method: "post",
        body: json,
        headers: {
            'Content-Type': 'application/json',
                },
        };
        const response = await fetch(locationUrl, fetchConfig);
        if (response.ok) {
            formTag.reset();
            const newLocation = await response.json();
            console.log(newLocation);
        }

    });

});
