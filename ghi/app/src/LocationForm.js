import React from 'react';

class LocationForm extends React.Component {
  
  // JS doesn't handle objects and methods the same way python does
  // If you refer to an object's method (like a property), JS won't
  // remember what object it came from. So if we are using a method as
  // an even handler (like HandleNameChange) we need this
  // constructor - bind 'thing'
  constructor(props){
    super(props);
    this.state = {
      name: '',
      city: '',
      roomCount: '',
      states: []
    };

    this.handleNameChange = this.handleNameChange.bind(this);
    this.handleRoomCountChange = this.handleRoomCountChange.bind(this);
    this.handleCityChange = this.handleCityChange.bind(this);
    this.handleStateChange = this.handleStateChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

    async componentDidMount() {
        const url = 'http://localhost:8000/api/states/';
    
        const response = await fetch(url);
    
        if (response.ok) {
            
            const data = await response.json();
        
            // the react way: 

            // set state adds the data to the React component
            // and reloads the component's HTML
            // the rest of the change is in the <select> tag in
            // the JSX html in the render() below
            this.setState({states: data.states});
            

            //the non-react way (below):
            // const selectTag = document.getElementById('state');
            // for (let state of data.states) {
            //     const option = document.createElement('option');
            //     option.value = state.abbreviation;
            //     option.innerHTML = state.name;
            //     selectTag.appendChild(option);
            // }
        }
    }

    handleNameChange(event) {
      // event is the event that occured, target is the tag that caused the event
      // value property is the value in the input
      const value = event.target.value;
      this.setState({name: value});
      //console.log(value); //just for fun
    }
    

    handleRoomCountChange(event) {
      const value = event.target.value;
      this.setState({roomCount: value});
      //console.log(value); //just for fun
    }


    handleCityChange(event) {
      const value = event.target.value;
      this.setState({city: value});
      //console.log(value); //just for fun
    }


    handleStateChange(event) {
      const value = event.target.value;
      this.setState({state: value});
      //console.log(value); //just for fun
    }


    async handleSubmit(event){
      event.preventDefault();
      const data = {...this.state};
      data.room_count = data.roomCount;

      console.log(data)
      
      delete data.roomCount;
      delete data.states;

      const locationUrl = 'http://localhost:8000/api/locations/';
      const fetchConfig = {
        method: "post",
        body: JSON.stringify(data),
        headers: {
          'Content-Type': 'application/json',
        },
      };
      const response = await fetch(locationUrl, fetchConfig);
      if (response.ok) {
        const newLocation = await response.json();
        console.log(newLocation);
    
        const cleared = {
          name: '',
          roomCount: '',
          city: '',
          state: '',
        };
        this.setState(cleared);
      }
    }

    render() {
        return (
            <div className="row">
            <div className="offset-3 col-6">
              <div className="shadow p-4 mt-4">
                <h1>Create a new location</h1>
                <form onSubmit={this.handleSubmit} id="create-location-form">
                  <div className="form-floating mb-3">
                    <input onChange={this.handleNameChange} 
                      value={this.state.name} 
                      placeholder="Name" 
                      required type="text" 
                      name ="name" 
                      id="name" 
                      className="form-control"/>
                    <label htmlFor="name">Name</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input onChange={this.handleRoomCountChange} value ={this.state.roomCount} placeholder="Room count" required type="number" name="room_count" id="room_count" className="form-control"/>
                    <label htmlFor="room_count">Room count</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input onChange={this.handleCityChange} value={this.state.city} placeholder="City" required type="text" name="city" id="city" className="form-control"/>
                    <label htmlFor="city">City</label>
                  </div>
                  <div className="mb-3">
                    <select onChange={this.handleStateChange} value={this.state.state} required name="state" id='state' className='form-select'>
                    <option value="">Choose a state</option>
                      {this.state.states.map(state => {
                       return (
                        <option key={state.abbreviation} value={state.abbreviation}>
                          {state.name}
                        </option>
                      );
                     })}
                    </select>
                  </div>
                  <button className="btn btn-primary">Create</button>
                </form>
              </div>
            </div>
          </div>
        );
    };
};

export default LocationForm;